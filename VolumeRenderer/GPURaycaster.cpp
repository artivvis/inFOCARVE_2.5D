#include "GPURaycaster.h"

using std::vector;
using namespace cv;
using namespace std;

GPURaycaster::GPURaycaster(int screenWidth, int screenHeight, VolumeDataset &volume)
{
	// Variables which will be used in shader
	maxRaySteps = 1000;
	rayStepSize = 0.005f;
	gradientStepSize = 0.005f;
	lightPosition = glm::vec3(-0.0f, -5.0f, 5.0f);

	xToon.ReadTexture();
}



// Messy but just inputs all uniforms to shader and raycasts using the passed in 3D texture and transfer function
void GPURaycaster::Raycast(GLuint currTexture3D, TransferFunction &transferFunction, GLuint shaderProgramID, Camera &camera, IplImage * inDepth, IplImage * inColor)
{
	int uniformLoc;

	glm::mat4 model_mat = glm::mat4(1.0f);

	uniformLoc = glGetUniformLocation (shaderProgramID, "proj");
	glUniformMatrix4fv (uniformLoc, 1, GL_FALSE, &camera.projMat[0][0]);

	uniformLoc = glGetUniformLocation (shaderProgramID, "view");
	glUniformMatrix4fv (uniformLoc, 1, GL_FALSE, &camera.viewMat[0][0]);

	uniformLoc = glGetUniformLocation (shaderProgramID, "model");
	glUniformMatrix4fv (uniformLoc, 1, GL_FALSE, &model_mat[0][0]);

	glActiveTexture (GL_TEXTURE0);
	uniformLoc = glGetUniformLocation(shaderProgramID,"volume");
	glUniform1i(uniformLoc,0);
	glBindTexture (GL_TEXTURE_3D, currTexture3D);

	uniformLoc = glGetUniformLocation(shaderProgramID,"camPos");
	glUniform3f(uniformLoc, camera.position.x, camera.position.y, camera.position.z);

	uniformLoc = glGetUniformLocation(shaderProgramID,"maxRaySteps");
	glUniform1i(uniformLoc, maxRaySteps);

	uniformLoc = glGetUniformLocation(shaderProgramID,"rayStepSize");
	glUniform1f(uniformLoc, rayStepSize);

	uniformLoc = glGetUniformLocation(shaderProgramID,"gradientStepSize");
	glUniform1f(uniformLoc, gradientStepSize);


	uniformLoc = glGetUniformLocation(shaderProgramID,"lightPosition");
	glUniform3f(uniformLoc, lightPosition.x, lightPosition.y, lightPosition.z);

	glActiveTexture (GL_TEXTURE1);
	uniformLoc = glGetUniformLocation(shaderProgramID,"transferFunc");
	glUniform1i(uniformLoc,1);
	glBindTexture (GL_TEXTURE_1D, transferFunction.tfTexture);

	glEnable(GL_TEXTURE_2D);
	glGenTextures(1,&rDepthTex2D);
	glBindTexture(GL_TEXTURE_2D,rDepthTex2D);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER, GL_LINEAR); 
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_REPLACE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT16, inDepth->width, inDepth->height, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_SHORT, inDepth->imageData);

	glActiveTexture (GL_TEXTURE2);
	uniformLoc = glGetUniformLocation(shaderProgramID,"depthTex");
	glUniform1i(uniformLoc,2);
	glBindTexture (GL_TEXTURE_2D, rDepthTex2D);


	glEnable(GL_TEXTURE_2D);
	glGenTextures(1,&rColorTex2D);
	glBindTexture(GL_TEXTURE_2D,rColorTex2D);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER, GL_LINEAR); 
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_REPLACE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, inColor->width, inColor->height, 0, GL_BGR, GL_UNSIGNED_BYTE, inColor->imageData);

	glActiveTexture (GL_TEXTURE3);
	uniformLoc = glGetUniformLocation(shaderProgramID,"colorTex");
	glUniform1i(uniformLoc,3);
	glBindTexture (GL_TEXTURE_2D, rColorTex2D);


	//short *** depth3dTex;
	//int depth3DTexz = 500;
	//depth3dTex = new short**[inDepth->width];  

	//for (int i = 0; i < inDepth->width; i ++) {  
	//	depth3dTex[i] = new short*[inDepth->height];  
	//	for (int j = 0; j < inDepth->height; j ++) {  
	//		depth3dTex[i][j] = new short[depth3DTexz];  
	//	}  
	//}  

	//for (int i = 0; i < inDepth->width; i ++) {  
	//	for (int j = 0; j < inDepth->height; j ++) {  
	//		for (int k = 0; k < depth3DTexz; k ++) {  
	//			depth3dTex[i][j][k] = 0;  
	//		}  
	//	}  
	//} 

	//for(int i = 0; i < inDepth->width; i++){
	//	for(int j = 0; j < inDepth->height; j++){
	//		int td = cvGet2D(inDepth,j,i).val[0];
	//		if(td>0 && td<depth3DTexz)
	//			depth3dTex[i][j][td] = 1;
	//	}
	//}

	//try{
	//	int sz[] = {inDepth->width,inDepth->height,depth3DTexz};
	//	Mat depth3dTexMat = Mat::zeros(3,sz,IPL_DEPTH_16U);


	//	for(int i = 0; i<inDepth->width; i++){
	//		for(int j = 0; j<inDepth->height; j++){
	//			int td = cvGet2D(inDepth,j,i).val[0];

	//		}
	//	}
	//}
	//catch (cv::Exception& e)
	//{
	//	cout<<e.msg<<endl;
	//}

	//glEnable(GL_TEXTURE_3D);
	//glGenTextures(1, &rDepthTex3D);
	//glBindTexture(GL_TEXTURE_3D, rDepthTex3D);
	//glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	//glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	//glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	//glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	//glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	//glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	//glTexImage3D(GL_TEXTURE_3D, 0, GL_R16F, inDepth->width, inDepth->height, depth3DTexz, 0, GL_RED, GL_UNSIGNED_SHORT, depth3dTex);
	//glPixelStoref(GL_UNPACK_SWAP_BYTES, false);


	//glActiveTexture (GL_TEXTURE4);
	//uniformLoc = glGetUniformLocation(shaderProgramID,"depthTex3D");
	//glUniform1i(uniformLoc,4);
	//glBindTexture (GL_TEXTURE_3D, rDepthTex3D);


	// Final render is the front faces of a cube which can then be raycasted into
	glBegin(GL_QUADS);

	// Front Face
	glVertex3f(-1.0f, -1.0f,  1.0f);
	glVertex3f( 1.0f, -1.0f,  1.0f);
	glVertex3f( 1.0f,  1.0f,  1.0f);
	glVertex3f(-1.0f,  1.0f,  1.0f);

	// Back Face
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glVertex3f(-1.0f,  1.0f, -1.0f);
	glVertex3f( 1.0f,  1.0f, -1.0f);
	glVertex3f( 1.0f, -1.0f, -1.0f);

	// Top Face
	glVertex3f(-1.0f,  1.0f, -1.0f);
	glVertex3f(-1.0f,  1.0f,  1.0f);
	glVertex3f( 1.0f,  1.0f,  1.0f);
	glVertex3f( 1.0f,  1.0f, -1.0f);

	// Bottom Face
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glVertex3f( 1.0f, -1.0f, -1.0f);
	glVertex3f( 1.0f, -1.0f,  1.0f);
	glVertex3f(-1.0f, -1.0f,  1.0f);

	// Right face
	glVertex3f( 1.0f, -1.0f, -1.0f);
	glVertex3f( 1.0f,  1.0f, -1.0f);
	glVertex3f( 1.0f,  1.0f,  1.0f);
	glVertex3f( 1.0f, -1.0f,  1.0f);

	// Left Face
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f,  1.0f);
	glVertex3f(-1.0f,  1.0f,  1.0f);
	glVertex3f(-1.0f,  1.0f, -1.0f);

	glEnd();

	glBindTexture(GL_TEXTURE_3D, 0);
}

// Messy but just inputs all uniforms to shader and raycasts using the passed in 3D texture and transfer function
void GPURaycaster::Raycast(GLuint currTexture3D, TransferFunction &transferFunction, GLuint shaderProgramID, Camera &camera)
{
	int uniformLoc;

	//from realsense tracking
	//glm::mat4 inTranslation = glm::translate(camera.translation);
	//float indegree = camera.rotation.w*180/3.14159265358979323846;
	//glm::mat4 inRotation = glm::rotate(indegree,glm::vec3(camera.rotation.x,camera.rotation.y,camera.rotation.z));

	glm::mat4 model_mat = glm::mat4(1.0f);
	//model_mat = model_mat*inRotation;

	uniformLoc = glGetUniformLocation (shaderProgramID, "proj");
	glUniformMatrix4fv (uniformLoc, 1, GL_FALSE, &camera.projMat[0][0]);

	//uniformLoc = glGetUniformLocation (shaderProgramID, "proj");
	//glUniformMatrix4fv (uniformLoc, 1, GL_FALSE, camera.proj);

	uniformLoc = glGetUniformLocation (shaderProgramID, "view");
	glUniformMatrix4fv (uniformLoc, 1, GL_FALSE, &camera.viewMat[0][0]);

	//uniformLoc = glGetUniformLocation (shaderProgramID, "view");
	//glUniformMatrix4fv (uniformLoc, 1, GL_FALSE, camera.modelView);

	//uniformLoc = glGetUniformLocation (shaderProgramID, "model");
	//glUniformMatrix4fv (uniformLoc, 1, GL_FALSE, camera.modelMatrix);

	uniformLoc = glGetUniformLocation (shaderProgramID, "model");
	glUniformMatrix4fv (uniformLoc, 1, GL_FALSE, &model_mat[0][0]);

	glActiveTexture (GL_TEXTURE0);
	uniformLoc = glGetUniformLocation(shaderProgramID,"volume");
	glUniform1i(uniformLoc,0);
	glBindTexture (GL_TEXTURE_3D, currTexture3D);

	uniformLoc = glGetUniformLocation(shaderProgramID,"camPos");
	glUniform3f(uniformLoc, camera.position.x, camera.position.y, camera.position.z);

	uniformLoc = glGetUniformLocation(shaderProgramID,"maxRaySteps");
	glUniform1i(uniformLoc, maxRaySteps);

	uniformLoc = glGetUniformLocation(shaderProgramID,"rayStepSize");
	glUniform1f(uniformLoc, rayStepSize);

	uniformLoc = glGetUniformLocation(shaderProgramID,"gradientStepSize");
	glUniform1f(uniformLoc, gradientStepSize);


	uniformLoc = glGetUniformLocation(shaderProgramID,"lightPosition");
	glUniform3f(uniformLoc, lightPosition.x, lightPosition.y, lightPosition.z);

	if(shaderProgramID == 1){
		glActiveTexture (GL_TEXTURE1);
		uniformLoc = glGetUniformLocation(shaderProgramID,"transferFunc");
		glUniform1i(uniformLoc,1);
		glBindTexture (GL_TEXTURE_1D, transferFunction.tfTexture);
	}

	if(shaderProgramID == 16){
		glActiveTexture (GL_TEXTURE1);
		uniformLoc = glGetUniformLocation(shaderProgramID,"xToonTexture");
		glUniform1i(uniformLoc,1);
		glBindTexture (GL_TEXTURE_2D, xToon.toonTexture);

		uniformLoc = glGetUniformLocation(shaderProgramID, "toonTextureWidth");
		glUniform1i(uniformLoc, xToon.textureWidth);

		uniformLoc = glGetUniformLocation(shaderProgramID, "toonTextureHeight");
		glUniform1i(uniformLoc, xToon.textureHeight);
	}

	//for realsense tracking test
	//glRotatef(camera.rotation.w,camera.rotation.x,camera.rotation.y,camera.rotation.z);
	//glTranslatef(camera.translation.x,camera.translation.y,camera.translation.z);

	//glEnable(GL_TEXTURE_2D);
	//glGenTextures(1,&rDepthTex2D);
	//glBindTexture(GL_TEXTURE_2D,rDepthTex2D);
	//glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER, GL_LINEAR); 
	//glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	//glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_REPLACE);
	//glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT16, inDepth->width, inDepth->height, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_SHORT, inDepth->imageData);

	//glActiveTexture (GL_TEXTURE2);
	//uniformLoc = glGetUniformLocation(shaderProgramID,"depthTex");
	//glUniform1i(uniformLoc,2);
	//glBindTexture (GL_TEXTURE_2D, rDepthTex2D);


	//glEnable(GL_TEXTURE_2D);
	//glGenTextures(1,&rColorTex2D);
	//glBindTexture(GL_TEXTURE_2D,rColorTex2D);
	//glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER, GL_LINEAR); 
	//glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	//glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_REPLACE);
	//glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, inColor->width, inColor->height, 0, GL_BGR, GL_UNSIGNED_BYTE, inColor->imageData);

	//glActiveTexture (GL_TEXTURE3);
	//uniformLoc = glGetUniformLocation(shaderProgramID,"colorTex");
	//glUniform1i(uniformLoc,3);
	//glBindTexture (GL_TEXTURE_2D, rColorTex2D);


	//short *** depth3dTex;
	//int depth3DTexz = 500;
	//depth3dTex = new short**[inDepth->width];  

	//for (int i = 0; i < inDepth->width; i ++) {  
	//	depth3dTex[i] = new short*[inDepth->height];  
	//	for (int j = 0; j < inDepth->height; j ++) {  
	//		depth3dTex[i][j] = new short[depth3DTexz];  
	//	}  
	//}  

	//for (int i = 0; i < inDepth->width; i ++) {  
	//	for (int j = 0; j < inDepth->height; j ++) {  
	//		for (int k = 0; k < depth3DTexz; k ++) {  
	//			depth3dTex[i][j][k] = 0;  
	//		}  
	//	}  
	//} 

	//for(int i = 0; i < inDepth->width; i++){
	//	for(int j = 0; j < inDepth->height; j++){
	//		int td = cvGet2D(inDepth,j,i).val[0];
	//		if(td>0 && td<depth3DTexz)
	//			depth3dTex[i][j][td] = 1;
	//	}
	//}

	//try{
	//	int sz[] = {inDepth->width,inDepth->height,depth3DTexz};
	//	Mat depth3dTexMat = Mat::zeros(3,sz,IPL_DEPTH_16U);


	//	for(int i = 0; i<inDepth->width; i++){
	//		for(int j = 0; j<inDepth->height; j++){
	//			int td = cvGet2D(inDepth,j,i).val[0];

	//		}
	//	}
	//}
	//catch (cv::Exception& e)
	//{
	//	cout<<e.msg<<endl;
	//}

	//glEnable(GL_TEXTURE_3D);
	//glGenTextures(1, &rDepthTex3D);
	//glBindTexture(GL_TEXTURE_3D, rDepthTex3D);
	//glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	//glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	//glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	//glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	//glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	//glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	//glTexImage3D(GL_TEXTURE_3D, 0, GL_R16F, inDepth->width, inDepth->height, depth3DTexz, 0, GL_RED, GL_UNSIGNED_SHORT, depth3dTex);
	//glPixelStoref(GL_UNPACK_SWAP_BYTES, false);


	//glActiveTexture (GL_TEXTURE4);
	//uniformLoc = glGetUniformLocation(shaderProgramID,"depthTex3D");
	//glUniform1i(uniformLoc,4);
	//glBindTexture (GL_TEXTURE_3D, rDepthTex3D);


	// Final render is the front faces of a cube which can then be raycasted into
	glBegin(GL_QUADS);

	// Front Face
	glVertex3f(-1.0f, -1.0f,  1.0f);
	glVertex3f( 1.0f, -1.0f,  1.0f);
	glVertex3f( 1.0f,  1.0f,  1.0f);
	glVertex3f(-1.0f,  1.0f,  1.0f);

	// Back Face
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glVertex3f(-1.0f,  1.0f, -1.0f);
	glVertex3f( 1.0f,  1.0f, -1.0f);
	glVertex3f( 1.0f, -1.0f, -1.0f);

	// Top Face
	glVertex3f(-1.0f,  1.0f, -1.0f);
	glVertex3f(-1.0f,  1.0f,  1.0f);
	glVertex3f( 1.0f,  1.0f,  1.0f);
	glVertex3f( 1.0f,  1.0f, -1.0f);

	// Bottom Face
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glVertex3f( 1.0f, -1.0f, -1.0f);
	glVertex3f( 1.0f, -1.0f,  1.0f);
	glVertex3f(-1.0f, -1.0f,  1.0f);

	// Right face
	glVertex3f( 1.0f, -1.0f, -1.0f);
	glVertex3f( 1.0f,  1.0f, -1.0f);
	glVertex3f( 1.0f,  1.0f,  1.0f);
	glVertex3f( 1.0f, -1.0f,  1.0f);

	// Left Face
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f,  1.0f);
	glVertex3f(-1.0f,  1.0f,  1.0f);
	glVertex3f(-1.0f,  1.0f, -1.0f);

	glEnd();

	glBindTexture(GL_TEXTURE_3D, 0);
}
