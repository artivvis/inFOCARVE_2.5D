#version 330

out vec4 FragColor;
in vec4 color;

in vec3 lightDirection;
in vec3 normal;
in vec4 eyeSpacePos;
in mat4 outProj;
in vec2 texCoords;
in float pointRadius;
in float depth;

void main()
{
	vec3 N;
	N.xy = (texCoords * vec2(2.0f, -2.0f)) + vec2(-1.0f, 1.0f);
	float r2 = dot(N.xy, N.xy);

	if (r2 > 1.0) 
		discard;

	N.z = sqrt(1.0f - r2);

	float directional = clamp(dot(normalize(N), normalize(-lightDirection)), 0.0, 1.0);
	float ambient = 0.5f;

//	gl_FragDepth = depth;

	directional = directional/2;
	FragColor = color*(directional + ambient);
	FragColor = vec4(1.0f, 1.0f, 1.0f, 1.0f);
}