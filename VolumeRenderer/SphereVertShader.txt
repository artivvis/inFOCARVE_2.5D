#version 330

in vec3 vPosition;
layout (location = 3) in vec4 vColor;
in vec2 vTexture;
in float pRadius;

out vec4 color;
out vec3 lightDirection;
out vec3 normal;
out vec4 eyeSpacePos;
out mat4 outProj;
out vec2 texCoords;
out float pointRadius;
out float depth;

uniform mat4 proj, view, model;

void main()
{
	lightDirection = mat3(view) * vec3(-1,-1,-1);
	color = vColor;
	outProj = proj;
	pointRadius = pRadius;

	texCoords = vTexture;

	eyeSpacePos = (view * model) * vec4(vPosition, 1.0f);

	vec4 clipSpacePos = proj * eyeSpacePos;

	depth = clipSpacePos.z / clipSpacePos.w;

	depth = (2.0f * 0.1f) / (20.0f + 0.1f -  depth * (20.0f - 0.1f));

	gl_Position = proj * view * model * vec4(vPosition.x,vPosition.y,vPosition.z,1.0);	
}