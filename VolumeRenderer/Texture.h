#ifndef TEXTURE_H
#define TEXTURE_H


#include "ShaderManager.h"


class Texture
{
public:
	GLuint ID;

	Texture(GLint intFormat, GLsizei width, GLsizei height, GLenum format, GLenum type, GLvoid *data);

};

#endif
