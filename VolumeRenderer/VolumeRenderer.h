#ifndef VOLUME_RENDERER_H
#define VOLUME_RENDERER_H


#include <windows.h>
#include "pxcprojection.h"
#include "pxcsensemanager.h"
#include "pxcmetadata.h"
#include "pxccapture.h"

#include "util_cmdline.h"
#include "util_render.h"
#include <conio.h>

#include "Camera.h"
#include "VolumeDataset.h"
#include "ShaderManager.h"
#include "OpenGLRenderer.h"
#include "Texture.h"
#include "BackgroundRenderer.h"
#include "Framebuffer.h"
#include <time.h>

//#include <GLAux.h>

#include "opencv2/opencv.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/core.hpp"
#include "opencv2/opencv_modules.hpp"
#include "opencv2/saliency.hpp"
#include "opencv2/core/utility.hpp"

#include <IMetaioSDKWin32.h>


class VolumeRenderer
{
public:
	Camera camera;
	GLuint shaderProgramID;
	ShaderManager shaderManager;
	VolumeDataset volume;
	OpenGLRenderer *renderer;

	clock_t oldTime;
	int currentTimestep;



	void Init(int screenWidth, int screenHeight);
	void Update(IplImage * inDepth, IplImage * inColor);
	void Update();

};


#endif