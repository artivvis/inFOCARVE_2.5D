﻿#include "VolumeRenderer.h"
#include "Cube.h"



#define SCREEN_WIDTH 640
#define SCREEN_HEIGHT 480
#define DEPTH_WIDTH 640
#define DEPTH_HEIGHT 480


VolumeRenderer volumeRenderer;
BackgroundRenderer mBackgroundRenderer;
ShaderManager mShaderManager;

//For RealSense Camera
PXCSenseManager *pp = NULL;
PXCCaptureManager *cm = NULL;
HBITMAP m_bitmap=NULL;
pxcStatus sts;
PXCVideoModule::DataDesc desc={};
HWND m_window;
PXCImage::ImageData data; // realsense color image
PXCImage::ImageData dataDepth; // realsense depth image
PXCCapture::Sample *sample = NULL;
PXCImage::ImageInfo iinfo;
PXCSession *session = NULL;
PXCProjection *projection = NULL;
PXCImage *tpxcimage = NULL;
PXCScenePerception *perception = NULL;
RealSense realSense;
PXCTracker *tracker; //for tracking a mask
pxcUID trackedID;


Camera mCamera;

UtilRender renderc(L"Color"), renderd(L"Depth");//realsense display

bool saveRealsense = FALSE;//save realsense input images to disk,"F:\\t_depth.jpg","F:\\t_color.jpg"
bool mouseMask = FALSE;//use mouse movement to edit the mask image

int countrun = 0;

GLuint g_texture = 0;
Texture *backgroundTex, *volumeTex;
Framebuffer *framebuffer;

//for metaio
metaio::IMetaioSDKWin32*	m_pMetaioSDK;
metaio::ISensorsComponent*	m_pSensors;
Cube*						m_pCube;

//For opencv
//IplImage* colorimg = NULL;
//IplImage *image = NULL;



// For mouse control
int mouseX = -1;//x coord of mouse
int mouseY = -1;//y coord of mouse
int xclickedAt = -1;
int yclickedAt = -1;

bool mouseisdown=false;  
bool loopr=false;  
int mx,my;  
int ry=10;  
int rx=10;  

using namespace cv;
using namespace saliency;


//initialize the RealSense Camera
void realSenseInit(){

	//image = cvLoadImage("F:\\Pic_de_neige_cordier_Face_E.jpg");
	//cvShowImage("texture",image);

	/* Creates an instance of the PXCSenseManager */
	pp = PXCSenseManager::CreateInstance();
	if (!pp) {
		wprintf_s(L"Unable to create the SenseManager\n");
	}

	cm=pp->QueryCaptureManager();
	pp->EnableStream(PXCCapture::STREAM_TYPE_COLOR, SCREEN_WIDTH, SCREEN_HEIGHT,30);
	pp->EnableStream(PXCCapture::STREAM_TYPE_DEPTH,DEPTH_WIDTH,DEPTH_HEIGHT,30);

	//for tracking a mask using realsense tracking
	//if (pp->EnableTracker() < PXC_STATUS_NO_ERROR)
	//{
	//	cout<< "Error Enable Tracker..."<<endl;
	//}

	//tracker=pp->QueryTracker();

	//if(tracker->Set2DTrackFromFile((const pxcCHAR*)L"H:\\VolumeRenderer\\Volume Renderer\\metaioman_target.png",trackedID) < PXC_STATUS_NO_ERROR)
	//{
	//	cout<< "Error loading Marker image..."<<endl;
	//}

	sts = pp->Init();
	pp->QueryCaptureManager()->QueryDevice()->SetMirrorMode( PXCCapture::Device::MirrorMode::MIRROR_MODE_DISABLED );

	session=PXCSession::CreateInstance();

	memset(&iinfo,0,sizeof(iinfo));
	iinfo.width=SCREEN_WIDTH;
	iinfo.height=SCREEN_HEIGHT;
	iinfo.format = PXCImage::PIXEL_FORMAT_DEPTH; //BGR order
	tpxcimage= session->CreateImage(&iinfo);


	projection=pp->QueryCaptureManager()->QueryDevice()->CreateProjection();



	//pp->EnableScenePerception();

	//perception  = pp->QueryScenePerception();
	//pp->PauseScenePerception(TRUE);

	//AUX_RGBImageRec *TextureImage; //±£´æÌùÍ¼Êý¾ÝµÄÖ¸Õë 
	//TextureImage = auxDIBImageLoad(("F:/Pic_de_neige_cordier_Face_E.jpg")); //ÔØÈëÌùÍ¼Êý¾Ý 
	//int textureWidth = TextureImage->sizeX; 
	//int textureHeight = TextureImage->sizeY;


}

//for test, it seems that realsense F200 currently does not support the Member Scene Perception api
void realSensePerception(){

	pp = PXCSenseManager::CreateInstance();
	pp->EnableScenePerception();
	perception=pp->QueryScenePerception();
	pp->Init();

	while(true){
		// Stream data
		while (pp->AcquireFrame(true)>=PXC_STATUS_NO_ERROR) {
			// Retrieve the emotion detection results if ready
			PXCScenePerception *sp2=pp->QueryScenePerception();
			sample = pp->QueryScenePerceptionSample();
			if (sp2) {
				if(sample->color && sample->depth){
					renderc.RenderFrame(sample->color);
					renderd.RenderFrame(tpxcimage);}
			}

			// Resume next frame processing
			pp->ReleaseFrame();
		}
	}

	// Clean up
	pp->Release();
}

//clear realsense memory, important
void realSenseRelease(){

	if(sample->color)sample->color->ReleaseAccess(&data);
	if(sample->depth)sample->depth->ReleaseAccess(&dataDepth);
	//DeleteObject(m_bitmap);
	pp->ReleaseFrame();



	//sample = NULL;
}

//convert hbitmap to opencv iplimage, does not work...
IplImage* hBitmap2Ipl(HBITMAP hBmp)
{
	BITMAP bmp; 
	::GetObject(hBmp,sizeof(BITMAP),&bmp);//hBmp-->bmp
	int nChannels = bmp.bmBitsPixel == 1 ? 1 : bmp.bmBitsPixel/8 ;
	int depth = bmp.bmBitsPixel == 1 ? IPL_DEPTH_1U : IPL_DEPTH_8U; 
	IplImage* img = cvCreateImage(cvSize(bmp.bmWidth,bmp.bmHeight),depth,nChannels); //cvCreateImageHeader
	//pBuffer = (char*)malloc(bmp.bmHeight*bmp.bmWidth*nChannels*sizeof(char));
	memcpy(img->imageData,(char*)(bmp.bmBits),bmp.bmHeight*bmp.bmWidth*nChannels);
	IplImage *dst = cvCreateImage(cvGetSize(img),img->depth,3);
	cvCvtColor(img,dst,CV_BGRA2BGR);
	cvReleaseImage(&img);
	return dst;
}

//save the input image to PXCCapture::Sample *sample
void getRealSenseColorImage(){


	//* Waits until new frame is available and locks it for application processing */
	sts=pp->AcquireFrame(true);

	if (sts<PXC_STATUS_NO_ERROR) {
		if (sts==PXC_STATUS_STREAM_CONFIG_CHANGED) {
			wprintf_s(L"Stream configuration was changed, re-initilizing\n");
			pp->Close();
		}                
	}

	sample = pp->QuerySample();
	//PXCImage::ImageData mCImageData;
	//sample->color->AcquireAccess(PXCImage::ACCESS_READ, &mCImageData);

	//PXCImage::ImageInfo info = sample->color->QueryInfo();

	if (sample->color->AcquireAccess(PXCImage::ACCESS_READ, PXCImage::PIXEL_FORMAT_RGB32, &data) >= PXC_STATUS_NO_ERROR)
	{
		//HWND hwndPanel = GetDlgItem(m_window, 1013);
		//HDC dc = GetDC(hwndPanel);
		//BITMAPINFO binfo;
		//memset(&binfo, 0, sizeof(binfo));
		//binfo.bmiHeader.biWidth = data.pitches[0]/4;
		//binfo.bmiHeader.biHeight = - (int)info.height;
		//binfo.bmiHeader.biBitCount = 32;
		//binfo.bmiHeader.biPlanes = 1;
		//binfo.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
		//binfo.bmiHeader.biCompression = BI_RGB;
		//Sleep(1);
		//m_bitmap = CreateDIBitmap(dc, &binfo.bmiHeader, CBM_INIT, data.planes[0], &binfo, DIB_RGB_COLORS);

		//ReleaseDC(hwndPanel, dc);
		//sample->color->ReleaseAccess(&data);
	}
}

//save the input depth image to PXCCapture::Sample *sample
void getRealSenseDepthImage(){

	//* Waits until new frame is available and locks it for application processing */
	sts=pp->AcquireFrame(true);

	if (sts<PXC_STATUS_NO_ERROR) {
		if (sts==PXC_STATUS_STREAM_CONFIG_CHANGED) {
			wprintf_s(L"Stream configuration was changed, re-initilizing\n");
			pp->Close();
		}                
	}

	sample = pp->QuerySample();
	//PXCImage::ImageData mCImageData;
	//sample->color->AcquireAccess(PXCImage::ACCESS_READ, &mCImageData);

	//PXCImage::ImageInfo info = sample->depth->QueryInfo();

	if (sample->depth->AcquireAccess(PXCImage::ACCESS_READ, PXCImage::PIXEL_FORMAT_DEPTH, &dataDepth) >= PXC_STATUS_NO_ERROR)
	{

	}
}

//get both depth and color
bool getRealSenseImages(){

	//* Waits until new frame is available and locks it for application processing */
	sts=pp->AcquireFrame(true);

	if (sts<PXC_STATUS_NO_ERROR) {
		if (sts==PXC_STATUS_STREAM_CONFIG_CHANGED) {
			wprintf_s(L"Stream configuration was changed, re-initilizing\n");
			pp->Close();
		}                
	}


	sample = pp->QuerySample();
	//PXCImage::ImageData mCImageData;
	//sample->color->AcquireAccess(PXCImage::ACCESS_READ, &mCImageData);

	//PXCImage::ImageInfo info = sample->depth->QueryInfo();

	if(sample && sample->depth && sample->color
		&& sample->depth->AcquireAccess(PXCImage::ACCESS_READ, PXCImage::PIXEL_FORMAT_DEPTH, &dataDepth) >= PXC_STATUS_NO_ERROR
		&& sample->color->AcquireAccess(PXCImage::ACCESS_READ, PXCImage::PIXEL_FORMAT_RGB32, &data) >= PXC_STATUS_NO_ERROR)
	{		
		return TRUE;	
	}
	else
	{		
		return FALSE;
	}

}

IplImage* imageData2IplImage(PXCImage::ImageData * inImg, int inWidth, int inHeight, int inChannels){

	IplImage *outImg = cvCreateImageHeader(cvSize(inWidth, inHeight), IPL_DEPTH_16U, inChannels);
	cvSetData(outImg, (short*)inImg->planes[0], inWidth*sizeof(short));
	//cv::Mat Img(outImg);
	//cvShowImage("image",outImg);
	return outImg;
	//cv::Mat outImg(outImg);
}

//convert opencv iplimage to opengl texture
GLuint ConvertIplToTexture(IplImage *image)
{
	GLuint texture;

	glGenTextures(1,&texture);
	glBindTexture(GL_TEXTURE_2D,texture);
	glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_DECAL);
	glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_REPEAT);
	glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_REPEAT);
	gluBuild2DMipmaps(GL_TEXTURE_2D,3,image->width,image->height,
		GL_BGR,GL_UNSIGNED_BYTE,image->imageData);

	return texture;
}

//update scene
void metaioUpdate()
{
	//m_pMetaioSDK->requestCameraImage();

	// Note: The metaio SDK itself does not render anything here because we initialized it with the NULL
	// renderer. The call is still necessary to get the camera image and update tracking.
	m_pMetaioSDK->render();

	// Store all OpenGL attributes that get changed so we can restore the state afterwards
	glPushAttrib(GL_ENABLE_BIT | GL_TRANSFORM_BIT | GL_TEXTURE_BIT | GL_DEPTH_BUFFER_BIT |
		GL_CURRENT_BIT);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Render camera image as background
	//m_pCameraImageRenderer->draw(m_screenAspect);

	// Render cube in front (if we're currently tracking)
	const metaio::TrackingValues &trackingValues = m_pMetaioSDK->getTrackingValues(1);

	if(trackingValues.quality > 0)
	{
		float modelMatrix[16];
		// preMultiplyWithStandardViewMatrix=false parameter explained below
		m_pMetaioSDK->getTrackingValues(1, modelMatrix, false, true);

		glEnable(GL_DEPTH_TEST);
		glDisable(GL_TEXTURE_2D);

		// With getTrackingValues(..., preMultiplyWithStandardViewMatrix=true), the metaio SDK would
		// calculate a model-view matrix, i.e. a standard look-at matrix (looking from the origin along
		// the negative Z axis) multiplied by the model matrix (tracking pose). Here we use our own
		// view matrix for demonstration purposes (parameter set to false), for instance if you have your
		// own camera implementation. Additionally, the cube is scaled up by factor 40 and translated
		// by 40 units in order to have its back face lie on the tracked image.
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();

		// Use typical view matrix (camera looking along negative Z axis, see previous hint)
		glLoadIdentity();

		// The order is important here: We first want to scale the cube, then put it 40 units
		// higher (because it's rendered from -1 to +1 on all axes, after scaling that's +-40)
		// so that its back face lies on the tracked image and move it into place
		// (transformation to the coordinate system of the tracked image).
		glMultMatrixf(modelMatrix); // MODEL_VIEW = LOOK_AT * MODEL
		glTranslatef(0, 0, 40);
		glScalef(40, 40, 40); // all sides of the cube then have dimension 80

		glMatrixMode(GL_PROJECTION);
		float projMatrix[16];

		// Use right-handed projection matrix
		m_pMetaioSDK->getProjectionMatrix(projMatrix, true);

		// Since we render the camera image ourselves, and there are devices whose screen aspect
		// ratio does not match the camera aspect ratio, we have to make up for the stretched
		// and cropped camera image. The CameraImageRenderer class gives us values by which
		// pixels should be scaled from the middle of the screen (e.g. getScaleX() > 1 if the
		// camera image is wider than the screen and thus its width is displayed cropped).
		projMatrix[0] *= 1.9;
		projMatrix[5] *= 2.5;

		glLoadMatrixf(projMatrix);

		m_pCube->render();
	}

	glPopAttrib();

	//SwapBuffers(m_pClientDC->m_hDC);

	//if(m_requestingClose)
	//	CFrameWnd::OnClose();
}

//init the saliency api
void saliencyInit(String type)
{
	Ptr<Saliency> saliencyAlgorithm = Saliency::create( "BinWangApr2014" );
	saliencyAlgorithm.dynamicCast<MotionSaliencyBinWangApr2014>()->setImagesize( SCREEN_WIDTH, SCREEN_HEIGHT );
	saliencyAlgorithm.dynamicCast<MotionSaliencyBinWangApr2014>()->init();
}

// Update renderer and check for Qt events
void MainLoop()
{

	if(getRealSenseImages()){
		tpxcimage = projection->CreateDepthImageMappedToColor(sample->depth,sample->color);

		PXCImage::ImageData tdata;
		tpxcimage->AcquireAccess(PXCImage::ACCESS_READ, PXCImage::PIXEL_FORMAT_DEPTH, &tdata);

		//to do saliency detection for rgb image
		//Mat backgroundColor8u4(SCREEN_HEIGHT,SCREEN_WIDTH,CV_8UC4,data.planes[0],data.pitches[0]);
		//Mat backgroundGray8u1;
		//try{

		//	Ptr<Saliency> saliencyAlgorithm = Saliency::create( "SPECTRAL_RESIDUAL" );
		//	//saliencyAlgorithm.dynamicCast<MotionSaliencyBinWangApr2014>()->setImagesize( SCREEN_WIDTH, SCREEN_HEIGHT );
		//	//saliencyAlgorithm.dynamicCast<MotionSaliencyBinWangApr2014>()->init();

		//	cvtColor( backgroundColor8u4, backgroundGray8u1, CV_BGRA2GRAY );

		//	Mat saliencyMap;
		//	saliencyAlgorithm->computeSaliency(backgroundGray8u1, saliencyMap ) ;

		//	//normalize(saliencyMap,saliencyMap,255,0,NORM_MINMAX);

		//	imshow("Gray image for saliency",backgroundGray8u1);
		//	imshow("Saliency image",saliencyMap);
		//}
		//catch (cv::Exception& e)
		//{
		//	cout<<e.msg<<endl;
		//}



		//to display the realsense images in the volume renderer cube fragment shader
		//IplImage * backgroundImage16u1 = cvCreateImageHeader(cvSize(SCREEN_WIDTH,SCREEN_HEIGHT),IPL_DEPTH_16U,1);  //for realsense depth image
		//IplImage * backgroundColorImage8u3 = cvCreateImageHeader(cvSize(SCREEN_WIDTH,SCREEN_HEIGHT),IPL_DEPTH_8U,3); //for realsense color image

		//IplImage * backgroundImage8u1 = cvCreateImage(cvSize(SCREEN_WIDTH,SCREEN_HEIGHT),IPL_DEPTH_8U,1);
		//IplImage * backgroundImage8u3 = cvCreateImage(cvSize(SCREEN_WIDTH,SCREEN_HEIGHT),IPL_DEPTH_8U,3);
		//IplImage * backgroundImage8u3Resized = cvCreateImage(cvSize(SCREEN_WIDTH,SCREEN_HEIGHT),IPL_DEPTH_8U,3);
		//IplImage * backgroundImage32f = cvCreateImage(cvSize(SCREEN_WIDTH,SCREEN_HEIGHT),IPL_DEPTH_32F,1);
		//
		//try{

		//	cvSetData(backgroundImage16u1,dataDepth.planes[0],dataDepth.pitches[0]);
		//	cvSetData(backgroundColorImage8u3,data.planes[0],data.pitches[0]);

		//	//min and max value of depth image
		//	double maxDepth = 0, minDepth = 0; 
		//	cvMinMaxLoc(backgroundImage16u1,&minDepth,&maxDepth);
		//	//cout << maxDepth<<endl;
		//	cvConvert(backgroundImage16u1,backgroundImage8u1);
		//	cvConvert(backgroundImage16u1,backgroundImage32f);
		//	cvMinMaxLoc(backgroundImage8u1,&minDepth,&maxDepth);
		//	cvNormalize(backgroundImage32f,backgroundImage32f,0.5,0.1,CV_MINMAX);
		//	//cvNormalize(backgroundImage8u1,backgroundImage8u1,255,0,CV_MINMAX);
		//	cvMinMaxLoc(backgroundImage32f,&minDepth,&maxDepth);

		//	cvCvtColor(backgroundImage8u1,backgroundImage8u3,CV_GRAY2BGR);
		//	cvResize(backgroundImage8u3,backgroundImage8u3Resized);

		//	//cout << maxDepth<<endl;
		//}
		//catch (cv::Exception& e)
		//{
		//	cout<<e.msg<<endl;
		//}

		//for tracking
		//PXCTracker::TrackingValues  trackData;

		//int trackedObjectNum = tracker->QueryNumberTrackingValues();
		//if(trackedObjectNum>0)
		//{
		//	cout<< "Tracking objects numbers: " <<trackedObjectNum <<endl; 

		//	if (tracker->QueryTrackingValues(trackedID,trackData) < PXC_STATUS_NO_ERROR)
		//	{
		//		cout<< "Error getting tracing values..."<<endl;
		//	}
		//}


		//for repairing the depth image using opencv
		//IplImage * depthImage16u1 = cvCreateImageHeader(cvSize(DEPTH_WIDTH,DEPTH_HEIGHT),IPL_DEPTH_16U,1);
		//IplImage * colorImage8u3  = cvCreateImageHeader(cvSize(SCREEN_WIDTH,SCREEN_HEIGHT),IPL_DEPTH_8U,4); //for realsense color image
		//IplImage * depthImage8u1  = cvCreateImage(cvSize(DEPTH_WIDTH,DEPTH_HEIGHT),IPL_DEPTH_8U,1);		
		//IplImage * dst = cvCreateImage(cvSize(DEPTH_WIDTH,DEPTH_HEIGHT),IPL_DEPTH_8U,1);

		//try{
		//	cvSetData(depthImage16u1,tdata.planes[0],tdata.pitches[0]);
		//	cvConvert(depthImage16u1,depthImage8u1);
		//	cvSetData(colorImage8u3,data.planes[0],data.pitches[0]);
		//	cvSet(dst,cvScalar(0));

		//save original data from realSense camera
		if(saveRealsense)
		{

			Mat depth16u1(SCREEN_HEIGHT,SCREEN_WIDTH,CV_16UC1,tdata.planes[0],tdata.pitches[0]);
			Mat color8u4(SCREEN_HEIGHT,SCREEN_WIDTH,CV_8UC4,data.planes[0],data.pitches[0]);

			cout<<"Saving realsense input images...";

			imwrite("F:\\t_depth.jpg",depth16u1);
			imwrite("F:\\t_color.jpg",color8u4);

			saveRealsense = FALSE;
			cout<<"Done!"<<endl;
		}


		//	// for fill holes in depth image
		//	//vector<vector<Point> > contours;
		//	//vector<Vec4i> hierarchy;
		//	//findContours( Mat(depthImage8u1, false), contours, hierarchy,CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE );

		//	//for(int i = 0; i<hierarchy.size();i++)
		//	//{
		//	//	if(hierarchy[i][3]>0)//if a contour has parent
		//	//	{
		//	//		Scalar color( rand()&255, rand()&255, rand()&255 );
		//	//		drawContours( Mat(dst, false), contours, i, cvScalar(200), CV_FILLED, 8, hierarchy );
		//	//	}
		//	//}

		//	//cvConvert(depthImage16u1,depthImage8u1);
		//	//cvInpaint(depthImage8u1,dst,depthImage8u1,10,INPAINT_TELEA);
		//	//cvConvert(depthImage8u1,depthImage16u1);
		//}
		//catch(cv::Exception& e)
		//{
		//	cout<<e.msg<<endl;
		//}

		//namedWindow( "contours", 1 );
		//imshow( "contours", Mat(dst, false) );

		//cvShowImage("inpaint result", depthImage8u1);


		//for metaio tracking		
		m_pMetaioSDK->setImage(metaio::ImageStruct(data.planes[0],SCREEN_WIDTH,SCREEN_HEIGHT,metaio::common::ECF_BGRA8,true,0));
		m_pMetaioSDK->render();

		metaio::TrackingValues &trackingValues = m_pMetaioSDK->getTrackingValues(1);
		if(trackingValues.quality > 0)
		{
			//trackingValues.rotation.setNoRotation();
			//trackingValues.rotation.setFromQuaternion(metaio::Vector4d(1.0,0.0,0.0,40));

			//m_pMetaioSDK->getTrackingValues(1, volumeRenderer.camera.modelMatrix, false, true);
			float modelMatrix[16];

			// preMultiplyWithStandardViewMatrix=false parameter explained below
			m_pMetaioSDK->getTrackingValues(1, modelMatrix, false, true);
			metaio::Vector3d tcamerapos = trackingValues.getInverseTranslation();



			volumeRenderer.camera.position.x = tcamerapos.x;
			volumeRenderer.camera.position.y = tcamerapos.y;
			volumeRenderer.camera.position.z = tcamerapos.z;
			volumeRenderer.camera.viewMat = glm::make_mat4(modelMatrix);


			//operation to camera is inverse to the matrix, 
			//rotate the model by 90 degrees for Mri-head
			//volumeRenderer.camera.position = glm::rotateX(volumeRenderer.camera.position,-90.0f);
			//volumeRenderer.camera.viewMat = glm::rotate(volumeRenderer.camera.viewMat,90.0f,glm::vec3(1.0,0.0,0.0));

			//for vismale data
			volumeRenderer.camera.position = glm::rotateX(volumeRenderer.camera.position,-180.0f);
			volumeRenderer.camera.viewMat = glm::rotate(volumeRenderer.camera.viewMat,180.0f,glm::vec3(1.0,0.0,0.0));
			volumeRenderer.camera.position = glm::rotateZ(volumeRenderer.camera.position,90.0f);
			volumeRenderer.camera.viewMat = glm::rotate(volumeRenderer.camera.viewMat,-90.0f,glm::vec3(0.0,0.0,1.0));


			//for the tree model Bonsai2
			//volumeRenderer.camera.position = glm::rotateX(volumeRenderer.camera.position,90.0f);
			//volumeRenderer.camera.viewMat = glm::rotate(volumeRenderer.camera.viewMat,-90.0f,glm::vec3(1.0,0.0,0.0));

			//for mribrain
			//volumeRenderer.camera.position = glm::rotateZ(volumeRenderer.camera.position,-180.0f);
			//volumeRenderer.camera.viewMat = glm::rotate(volumeRenderer.camera.viewMat,180.0f,glm::vec3(0.0,0.0,1.0));

			//move up the model to make it stand just on the marker
			//100 is scale factor for the whole model
			int scaleFactor = 200;
			volumeRenderer.camera.position.y -= scaleFactor;
			volumeRenderer.camera.viewMat = glm::translate(volumeRenderer.camera.viewMat,glm::vec3(0.0,scaleFactor,0.0));

			//move the model to fit real image model
			//int transX = 30, transY = -830, transZ = 280; // for Bonsai2
			//int transX = 30, transY = 400, transZ = -280; // for mri-head
			//int transX = 30, transY = -400, transZ = -150; // for mribrain
			int transX = 300, transY = -200, transZ = -500; // for VisMale
			volumeRenderer.camera.position.x -= transX;
			volumeRenderer.camera.position.y -= transY;
			volumeRenderer.camera.position.z -= transZ;
			volumeRenderer.camera.viewMat = glm::translate(volumeRenderer.camera.viewMat,glm::vec3(transX,transY,transZ));

			//scale the model in all XYZ axis
			//sequence is important, trans first scale second
			volumeRenderer.camera.position.x /= scaleFactor;
			volumeRenderer.camera.position.y /= scaleFactor;
			volumeRenderer.camera.position.z /= scaleFactor;
			volumeRenderer.camera.viewMat = glm::scale(volumeRenderer.camera.viewMat,glm::vec3(scaleFactor,scaleFactor,scaleFactor));




			float projMatrix[16];
			// Use right-handed projection matrix
			m_pMetaioSDK->getProjectionMatrix(projMatrix, true);
			volumeRenderer.camera.projMat = glm::make_mat4(projMatrix);

			//volumeRenderer.camera.Update();



			//volumeRenderer.camera.Rotate(-trackingValues.rotation.getQuaternion().w*180/3.14159265358979323846);
			//			glm::vec4 t_rotation;
			//			t_rotation.x = trackingValues.rotation.getQuaternion().x;
			//			t_rotation.y = trackingValues.rotation.getQuaternion().y;
			//			t_rotation.z = trackingValues.rotation.getQuaternion().z;
			//			t_rotation.w = trackingValues.rotation.getQuaternion().w*180/3.14159265358979323846;
			//			volumeRenderer.camera.RotateAll(t_rotation);

		}


		glClearColor(0.0f,0.0f,0.0f,0.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glBindTexture(GL_TEXTURE_2D,backgroundTex->ID);
		glTexSubImage2D(GL_TEXTURE_2D,0,0,0,SCREEN_WIDTH,SCREEN_HEIGHT,GL_BGRA,GL_UNSIGNED_BYTE,data.planes[0]); //for color texture
		//glTexSubImage2D(GL_TEXTURE_2D,0,0,0,SCREEN_WIDTH,SCREEN_HEIGHT,GL_BGR,GL_UNSIGNED_BYTE,backgroundImage8u3Resized->imageData); // for depth texture


		glBindTexture(GL_TEXTURE_2D,0);

		framebuffer->Bind();

		//glClearColor(0.0f,0.0f,0.0f,0.0f);
		//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		//mCamera.Update();

		framebuffer->ChangeTexture(volumeTex->ID);

		//for tracking using realsense tracking api
		//if(trackedObjectNum>0)
		//{
		//	//volumeRenderer.camera.Translate(glm::vec3(trackData.translation.x,trackData.translation.y,trackData.translation.z));
		//	//volumeRenderer.camera.Rotate(trackData.rotation.y);
		//	//volumeRenderer.camera.RotateAll(glm::vec4(trackData.rotation.w,trackData.rotation.x,trackData.rotation.y,trackData.rotation.z));
		//	volumeRenderer.camera.rotation = glm::vec4(trackData.rotation.x,trackData.rotation.y,trackData.rotation.z,trackData.rotation.w);
		//	volumeRenderer.camera.translation = glm::vec3(trackData.translation.x,trackData.translation.y,trackData.translation.z);
		//}
		//volumeRenderer.Update(backgroundImage16u1,backgroundColorImage8u3);
		volumeRenderer.Update();

		//volume opengl texture to opencv mat format

		Mat flipped(SCREEN_HEIGHT,SCREEN_WIDTH,CV_8UC1);
		try
		{
			Mat volumeImg(SCREEN_HEIGHT,SCREEN_WIDTH,CV_8UC3);
			glPixelStorei(GL_PACK_ALIGNMENT, (volumeImg.step & 3) ? 1 : 4);
			glPixelStorei(GL_PACK_ROW_LENGTH, volumeImg.step/volumeImg.elemSize());
			glReadPixels(0, 0, volumeImg.cols, volumeImg.rows, GL_BGR, GL_UNSIGNED_BYTE, volumeImg.data);


			flip(volumeImg, volumeImg, 0);
			imshow("volume only texture", volumeImg);
			cvtColor(volumeImg,flipped,CV_BGR2GRAY);

			Mat edgeDetec;
			Laplacian( flipped, edgeDetec, CV_8UC1, 3, 1, 0, BORDER_DEFAULT );  
			imshow("edge detection of volume image", edgeDetec);

			threshold(flipped,flipped,1,255,THRESH_BINARY);
			imshow("mask image", flipped);

		}
		catch(cv::Exception& e)
		{
			cout<<e.msg<<endl;
		}


		framebuffer->Unbind();

		//if(realSense.Update())
		//	realSense.DrawPoints(mCamera, volumeRenderer.shaderManager);



		//raw depth to depth for perception use				
		Mat depth16u1CutEqualized;
		try
		{
			Mat depth16u1(SCREEN_HEIGHT,SCREEN_WIDTH,CV_16UC1,tdata.planes[0],tdata.pitches[0]);
			Mat depth8u1;
			Mat depth8u1Opt;
			Mat depth16u1Cut;

			//double minVal, maxVal;
			//minMaxLoc(depth16u1, &minVal, &maxVal); //find minimum and maximum intensities
			//cout<<"min: "<<minVal<<", Max: "<< maxVal<<endl;
			//normalize(depth16u1,depth16u1,0,255,NORM_MINMAX);


			//depth16u1.convertTo(tDepth,CV_8UC1);
			//tDepth.copyTo(depth8u1,flipped);

			depth16u1.copyTo(depth16u1Cut,flipped);
			normalize(depth16u1Cut,depth16u1Cut,0,255,NORM_MINMAX);
			depth16u1Cut.convertTo(depth8u1,CV_8UC1);			
			equalizeHist(depth8u1,depth8u1);
			//cvSmooth( &(IplImage(depth8u1)), &(IplImage(depth8u1)), CV_GAUSSIAN, 7,7 );
			morphologyEx(depth8u1,depth8u1,MORPH_CLOSE,Mat(3,3,CV_8U),Point(-1,-1),5);
			dilate(depth8u1,depth8u1,Mat(5,5,CV_8U),Point(-1,-1),4);

			//draw a circle around mouse in the mask image
			if(mouseX >0 && mouseY>0)
			{
				circle(depth8u1,Point2d(mouseX,mouseY),50,1,CV_FILLED);
			}


			depth8u1.copyTo(depth8u1Opt,flipped);
			depth8u1Opt.convertTo(depth16u1CutEqualized,CV_16UC1);



			//Mat t1;
			//multiply(flipped,depth8u1,t1);
			//imshow("volume only binary", flipped);
			imshow("mask",depth8u1);
		}
		catch(cv::Exception& e)
		{
			cout<<e.msg<<endl;
		}


		int shaderID = volumeRenderer.shaderManager.UseShader(BackgroundShader);
		mBackgroundRenderer.DrawBackground(shaderID,backgroundTex->ID,volumeTex->ID,depth16u1CutEqualized.data);
		//mBackgroundRenderer.DrawBackground(shaderID,backgroundTex->ID,volumeTex->ID);


		renderc.RenderFrame(sample->color);
		renderd.RenderFrame(tpxcimage);//aligned depth image
		//renderd.RenderFrame(sample->depth);

		//		cvReleaseImageHeader(&backgroundImage16u1);
		//		cvReleaseImageHeader(&backgroundColorImage8u3);
		//		cvReleaseImage(&backgroundImage8u1);
		//		cvReleaseImage(&backgroundImage8u3);
		//		cvReleaseImage(&backgroundImage8u3Resized);

		if(tpxcimage)tpxcimage->ReleaseAccess(&tdata);
		if(tpxcimage)tpxcimage->Release();

		//cvReleaseImage(&depthImage8u1);
		//cvReleaseImage(&dst);
		//cvReleaseImageHeader(&depthImage16u1);
		//cvReleaseImageHeader(&colorImage8u3);


	}
	realSenseRelease();

	glutSwapBuffers();

}

//get frame rate
float g_fps( void (*func)(void), int n_frame )
{
	clock_t start, finish;
	int i;
	float fps;

	printf( "Performing benchmark, please wait..." );
	start = clock();
	for( i=0; i<n_frame; i++ )
	{
		func();
	}
	printf( "done!\n" );
	finish = clock();

	fps = float(n_frame)/(finish-start)*CLOCKS_PER_SEC;
	return fps;
}

// General Keyboard Input
void KeyboardFunc (unsigned char key, int xmouse, int ymouse)
{
	switch(key)
	{

	case 27:
		exit(0);
		break;
	case 102://ascii code of keyboard "f", show frame rate
		cout<<g_fps(MainLoop,50)<<endl;
		break;
	case 115://ascii code of keyboard "s", save realsense images to disk
		saveRealsense = TRUE;
		break;
	case 105://ascii code of "i", use mouse to control mask
		if(mouseMask == FALSE)
			mouseMask = TRUE;
		else
			mouseMask = FALSE;
		break;

	}
}



// Keyboad Special Keys Input
void SpecialFunc(int key, int x, int y)
{
	switch(key)
	{
	case GLUT_KEY_UP:
		volumeRenderer.camera.position.z -= 0.5f;
		break;
	case GLUT_KEY_LEFT:
		volumeRenderer.camera.position.x -= 0.5f;
		break;
	case GLUT_KEY_DOWN:
		volumeRenderer.camera.position.z += 0.5f;
		break;
	case GLUT_KEY_RIGHT:
		volumeRenderer.camera.position.x += 0.5f;
		break;
	case GLUT_KEY_PAGE_UP:
		volumeRenderer.camera.position.y += 0.5f;
		break;
	case GLUT_KEY_PAGE_DOWN:
		volumeRenderer.camera.position.y -= 0.5f;
		break;
	}
	glutPostRedisplay();
}


// Mouse drags to control camera
void MouseMove(int x, int y) 
{ 	


	if (xclickedAt >= 0)
	{
		volumeRenderer.camera.Rotate((float)(xclickedAt - x));
		xclickedAt = x;
	}


	if (yclickedAt >= 0)
	{
		volumeRenderer.camera.Translate(glm::vec3(0.0f, (y - yclickedAt) * 0.1f, 0.0f));
		yclickedAt = y;
	}
}

//get mouse position
void GetMousePos(int x, int y)
{
	mouseX = x;
	mouseY = y;
}


// Mouse clicks to initialize drags
void MouseButton(int button, int state, int x, int y) 
{
	if (button == GLUT_RIGHT_BUTTON) 
	{
		if (state == GLUT_UP)
		{
			xclickedAt = -1;
			yclickedAt = -1;
		}
		else
		{
			xclickedAt = x;
			yclickedAt = y;
		}
	}
}


// Mouse wheel to zoom camera
void MouseWheel(int wheel, int direction, int x, int y) 
{
	volumeRenderer.camera.Zoom(-direction * 0.2f);	
}


void reshape(int w, int h) { 
	glViewport (0, 0, (GLsizei)w, (GLsizei)h);//set viewpoint  
	glMatrixMode (GL_PROJECTION);//specify which matrix is the current matrix  
	glLoadIdentity ();//replace the current matrix with the identity matrix  
	gluPerspective (60, (GLfloat)w / (GLfloat)h, 1.0, 100.0);  
	glMatrixMode (GL_MODELVIEW);  
}

//display Realsense depth as 3D points in OpenGL
void depth3DLoop(){

	getRealSenseImages();

	if(dataDepth.pitches[0] && data.pitches[0]){

		IplImage * tDepth = cvCreateImageHeader(cvSize(DEPTH_WIDTH,DEPTH_HEIGHT),IPL_DEPTH_16U,1);
		IplImage *image = cvCreateImageHeader(cvSize(SCREEN_WIDTH,SCREEN_HEIGHT),IPL_DEPTH_8U,3);
		if(countrun>=418)
		{
			cout<<"";
		}
		try{
			//convert pxcimage.depth to opencv format

			cvSetData(tDepth,(short*)dataDepth.planes[0],dataDepth.pitches[0]);
			cvNormalize(tDepth,tDepth,255,0,CV_MINMAX);
		}
		catch (cv::Exception& e)
		{
			cout<<e.msg<<endl;
			cout<<"part3"<<endl;

		}
		try{
			//show the depth image in opencv window
			IplImage * timage = cvCreateImage(cvSize(DEPTH_WIDTH,DEPTH_HEIGHT),8,1);
			cvConvert(tDepth,timage);
			cvShowImage("depth opencv",timage);
			cvReleaseImage(&timage);
		}
		catch (cv::Exception& e)
		{
			cout<<e.msg<<endl;
			cout<<"part1"<<endl;

		}

		try{
			//convert pxcimage.depth to iplimage and show in opencv window

			cvSetData(image,(short*)data.planes[0],data.pitches[0]);
			cvShowImage("color opencv",image);
			//	cvReleaseImage(&image);
		}
		catch (cv::Exception& e)
		{
			cout<<e.msg<<endl;
			cout<<"part2"<<endl;
		}

		//Create Color Image Mapped To Depth
		tpxcimage = projection->CreateDepthImageMappedToColor(sample->depth,sample->color);
		PXCImage::ImageInfo info0 = sample->depth->QueryInfo();
		PXCImage::ImageInfo info1 = sample->color->QueryInfo();

		if (tpxcimage)
		{
			PXCImage::ImageInfo info = tpxcimage->QueryInfo();
			//	if(info.height>0){

			//convert the mapped image to opencv format
			PXCImage::ImageData tdata;
			tpxcimage->AcquireAccess(PXCImage::ACCESS_READ, PXCImage::PIXEL_FORMAT_DEPTH, &tdata);
			IplImage * mappedColorImage = cvCreateImageHeader(cvSize(SCREEN_WIDTH,SCREEN_HEIGHT),IPL_DEPTH_16U,1);
			cvSetData(mappedColorImage,(char*)tdata.planes[0],tdata.pitches[0]);
			//cvShowImage("mapped depth image opencv",mappedColorImage);


			//min and max value of depth image
			//double maxDepth = 0, minDepth = 0; 
			//cvMinMaxLoc(&tDepth,&minDepth,&maxDepth);

			//glPushMatrix();
			//glLoadIdentity();

			glClear(GL_COLOR_BUFFER_BIT);  
			glLoadIdentity();
			gluLookAt (0.0, 0.0, 7.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);  
			//gluLookAt(0,0,2,0.0,0.0,0.0,0.0,1.0,0.0);      //

			//to invert the image  
			glRotatef(0,0,1,0);  
			glRotatef(0-180,1,0,0);  

			float imageCenterX=SCREEN_WIDTH*.5;  
			float imageCenterY=SCREEN_HEIGHT*.5;  
			float x,y,z,scalar = 100;  

			glPointSize(1.0f);
			//glColor3f(1.0,1.0,1.0);    
			glBegin(GL_POINTS);
			for (int i=0;i<SCREEN_HEIGHT;i++)
			{
				for(int j=0;j<SCREEN_WIDTH;j++){
					float posx=0,posy=0;
					posx = 2*(float)j/SCREEN_WIDTH -1.0;
					posy = 2*(float)i/SCREEN_HEIGHT -1.0;
					float pixelDepth = cvGet2D(mappedColorImage,i,j).val[0];

					x=((float)j-imageCenterX)/scalar;  
					y=((float)i-imageCenterY)/scalar;  
					z=(pixelDepth)/scalar; 
					//glColor3f(cvGet2D(image,i,j).val[2]/255,cvGet2D(image,i,j).val[1]/255,cvGet2D(image,i,j).val[0]/255);
					glColor3f(pixelDepth/255,pixelDepth/255,pixelDepth/255);
					glVertex3f(x,y,z);
				}
			}

			glEnd();
			//glPopMatrix();

			tpxcimage->ReleaseAccess(&tdata);
			tpxcimage->Release();
			//cvReleaseImage(&mappedColorImage);
			glFlush();
			glutSwapBuffers();
			cout<<"glok"<<countrun++<<endl;
		}

		//projection->Release();
		//session->Release();
	}

	realSenseRelease();



}

//display Realsense input
void realSenseLoop(){

	//HBITMAP m_bitmap=NULL;
	////* Waits until new frame is available and locks it for application processing */
	//sts=pp->AcquireFrame(false);

	//if (sts<PXC_STATUS_NO_ERROR) {
	//	if (sts==PXC_STATUS_STREAM_CONFIG_CHANGED) {
	//		wprintf_s(L"Stream configuration was changed, re-initilizing\n");
	//		pp->Close();
	//	}                
	//}

	//const PXCCapture::Sample *sample = pp->QuerySample();
	////PXCImage::ImageData mCImageData;
	////sample->color->AcquireAccess(PXCImage::ACCESS_READ, &mCImageData);
	//
	//PXCImage::ImageInfo info = sample->color->QueryInfo();
	//PXCImage::ImageData data;
	//if (sample->color->AcquireAccess(PXCImage::ACCESS_READ, PXCImage::PIXEL_FORMAT_RGB32, &data) >= PXC_STATUS_NO_ERROR)
	//{
	//	//HWND hwndPanel = GetDlgItem(m_window, 1013);
	//	//HDC dc = GetDC(hwndPanel);
	//	//BITMAPINFO binfo;
	//	//memset(&binfo, 0, sizeof(binfo));
	//	//binfo.bmiHeader.biWidth = data.pitches[0]/4;
	//	//binfo.bmiHeader.biHeight = - (int)info.height;
	//	//binfo.bmiHeader.biBitCount = 32;
	//	//binfo.bmiHeader.biPlanes = 1;
	//	//binfo.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	//	//binfo.bmiHeader.biCompression = BI_RGB;
	//	//Sleep(1);
	//	//m_bitmap = CreateDIBitmap(dc, &binfo.bmiHeader, CBM_INIT, data.planes[0], &binfo, DIB_RGB_COLORS);

	//	//ReleaseDC(hwndPanel, dc);
	//	//sample->color->ReleaseAccess(&data);
	//}

	//BITMAP bitmap ;
	//GetObject(m_bitmap, sizeof(bitmap), &bitmap);
	//image = hBitmap2Ipl(m_bitmap);
	//DeleteObject(m_bitmap);

	//	   PXCImage::ImageData depthImageData;
	//   sample->color->AcquireAccess(PXCImage::ACCESS_READ_WRITE, &depthImageData);

	//image = cvCreateImageHeader(cvSize(SCREEN_WIDTH,SCREEN_HEIGHT),IPL_DEPTH_8U ,3);
	//cvSetData(image, (short*)data.planes[0], SCREEN_WIDTH*3*sizeof(IPL_DEPTH_8U));

	//cvCvtColor(image,image,CV_RGB2BGR);
	//cvSaveImage("F:\\t_.png",image);

	//cvShowImage("test",image);

	//glColor3f(1.0,0.0,1.0);
	//	glShadeModel(GL_FLAT); 
	//glEnable(GL_TEXTURE_2D); 
	//glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR); // 
	//glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR); //
	//glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_REPLACE);
	//	glGenTextures(1, &g_texture); 
	//glBindTexture(GL_TEXTURE_2D, g_texture); 

	//getRealSenseColorImage();
	getRealSenseDepthImage();

	//glLoadIdentity();
	glEnable(GL_TEXTURE_2D);
	glGenTextures(1,&g_texture);
	glBindTexture(GL_TEXTURE_2D,g_texture);

	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER, GL_LINEAR); 
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_REPLACE);

	//glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, SCREEN_WIDTH, SCREEN_HEIGHT, 0, GL_BGRA, GL_UNSIGNED_BYTE, dataDepth.planes[0]);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32F, DEPTH_WIDTH, DEPTH_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_FLOAT, dataDepth.planes[0]);

	glClear(GL_COLOR_BUFFER_BIT);   


	glBegin(GL_QUADS); 
	//	glColor3f(1.0,1.0,1.0);
	glTexCoord2f(0,0);glVertex2f(-1,1); 
	glTexCoord2f(1,0);glVertex2f(1,1); 
	glTexCoord2f(1,1);glVertex2f(1,-1); 
	glTexCoord2f(0,1);glVertex2f(-1,-1); 
	glEnd();

	realSenseRelease();

	//sample->color->ReleaseAccess(&data);
	//DeleteObject(m_bitmap);
	//pp->ReleaseFrame();
	//cvReleaseImage(&image);
	//glBegin(GL_POLYGON);
	//glColor3f(1.0,0.0,1.0);
	//glVertex3f(0.0, 0.0, 0.0);
	//glVertex3f(0.5, 0.0, 0.0);
	//glVertex3f(0.5, 0.5, 0.0);
	//glVertex3f(0.0, 0.5, 0.0);
	//glEnd();
	//glFlush();
	//glutSwapBuffers();
	//colorimg = cvCreateImageHeader(cvSize(SCREEN_WIDTH, SCREEN_HEIGHT), 8, 3);
	//cvSetData(colorimg, (uchar*)(mCImageData.planes[0]), SCREEN_WIDTH*3*sizeof(uchar));
	//cv::Mat image(colorimg);
	//GLubyte* pixeldata;
	//pixeldata =gl::Texture(cv::fromOcv(image)); //Here's how to get to OpenGL

	//glDrawPixels((GLsizei)((*sample->color).),sample->color.length,GL_BGR_EXT,GL_UNSIGNED_BYTE,);
	glFlush();
	glutSwapBuffers();

	//cvReleaseImage(&image);
}


void displayMe(void)
{
	glClear(GL_COLOR_BUFFER_BIT);
	glBegin(GL_POLYGON);
	glVertex3f(0.0, 0.0, 0.0);
	glVertex3f(0.5, 0.0, 0.0);
	glVertex3f(0.5, 0.5, 0.0);
	glVertex3f(0.0, 0.5, 0.0);
	glEnd();
	glFlush();

}

void init(void)
{
	glClearColor(0.0,0.0,0.0,0.0);     //
	glShadeModel(GL_FLAT);

}

void timer(int p)  
{  
	ry-=5;  
	//marks the current window as needing to be redisplayed.  
	glutPostRedisplay();  
	if (loopr)  
		glutTimerFunc(200,timer,0);  
}  

void mouse(int button, int state, int x, int y)  
{  
	if(button == GLUT_LEFT_BUTTON)  
	{  
		if(state == GLUT_DOWN)  
		{  
			mouseisdown=true;  
			loopr=false;  
		}  
		else  
		{  
			mouseisdown=false;  
		}  
	}  

	if (button== GLUT_RIGHT_BUTTON)  
		if(state == GLUT_DOWN)  
		{  
			loopr=true;   
			glutTimerFunc(200,timer,0);  
		}  
}  

void motion(int x, int y)  
{  
	if(mouseisdown==true)  
	{  
		ry+=x-mx;  
		rx+=y-my;  
		mx=x;  
		my=y;  
		glutPostRedisplay();  
	}  
}  

void special(int key, int x, int y)  
{  
	switch(key)  
	{  
	case GLUT_KEY_LEFT:  
		ry-=5;  
		glutPostRedisplay();  
		break;  
	case GLUT_KEY_RIGHT:  
		ry+=5;  
		glutPostRedisplay();  
		break;  
	case GLUT_KEY_UP:  
		rx+=5;  
		glutPostRedisplay();  
		break;  
	case GLUT_KEY_DOWN:  
		rx-=5;  
		glutPostRedisplay();  
		break;  
	}  
}  

//for test, seems not working with realsense ini together
void initMetaioSDK()
{
	// Create the SDK
	m_pMetaioSDK = metaio::CreateMetaioSDKWin32();
	m_pMetaioSDK->initializeRenderer(0, 0, metaio::ESCREEN_ROTATION_0, metaio::ERENDER_SYSTEM_NULL);

	m_pSensors = metaio::CreateSensorsComponent();
	m_pMetaioSDK->registerSensorsComponent(m_pSensors);

	// Since the metaio SDK may capture in YUV for performance reasons, we
	// enforce RGB capturing here to make it easier for us to handle the camera image
	//metaio::stlcompat::Vector<metaio::Camera> cameras = m_pMetaioSDK->getCameraList();
	//if (cameras.size()>0)
	//{
	//	cameras[0].yuvPipeline = false;
	//	m_pMetaioSDK->startCamera(cameras[0]);
	//}
	//else
	//{
	//	fprintf(stderr, "Unable to find a camera attached to the system\n");
	//}

	//m_pCameraImageRenderer = new CameraImageRenderer();

	// Set the callback to this class
	//m_pMetaioSDK->registerCallback(this);

	if(!m_pMetaioSDK->setTrackingConfiguration(metaio::Path::fromFSEncoding("../TrackingData_MarkerlessFast.xml")))
		fprintf(stderr, "Failed to set tracking configuration\n");
	else
		printf("Successfully set tracking configuration!\n");
}


int main(int argc, char *argv[])
{
	//mCamera.Init(SCREEN_WIDTH,SCREEN_HEIGHT);
	realSenseInit();
	initMetaioSDK();
	// Set up the window
	glutInit(&argc, argv);

	//For display Realsense depth in 3D
	//glutInitDisplayMode(GLUT_DOUBLE|GLUT_RGB);
	//glutInitWindowSize(SCREEN_WIDTH, SCREEN_HEIGHT);
	//glutCreateWindow("Depth 3D OpenGL");
	//init();
	//glutDisplayFunc(depth3DLoop);
	//glutIdleFunc(depth3DLoop);
	//glutReshapeFunc(reshape);

	////For display Realsense input
	//glutInitDisplayMode(GLUT_DOUBLE|GLUT_RGB);
	//glutInitWindowSize(DEPTH_WIDTH, DEPTH_HEIGHT);
	//glutCreateWindow("Camera");
	//glutDisplayFunc(realSenseLoop);
	//glutIdleFunc(realSenseLoop);
	//glutReshapeFunc(reshape);


	glutInitDisplayMode(GLUT_DOUBLE|GLUT_RGB);
	glutInitWindowSize(SCREEN_WIDTH, SCREEN_HEIGHT);
	glutCreateWindow("Volume Renderer");



	// A call to glewInit() must be done after glut is initialized!
	GLenum res = glewInit();
	// Check for any errors
	if (res != GLEW_OK) 
	{
		fprintf(stderr, "Error: '%s'\n", glewGetErrorString(res));
		return 1;
	}


	// Initialize Renderer

	volumeRenderer.Init(SCREEN_WIDTH, SCREEN_HEIGHT);	
	mBackgroundRenderer.Init();


	// Specify glut input functions
	glutKeyboardFunc(KeyboardFunc);
	glutSpecialFunc(SpecialFunc);
	glutMouseFunc(MouseButton);
	glutMotionFunc(MouseMove);
	glutMouseWheelFunc(MouseWheel);

	glutPassiveMotionFunc(GetMousePos);


	backgroundTex = new Texture(GL_RGBA,SCREEN_WIDTH,SCREEN_HEIGHT, GL_BGRA,GL_UNSIGNED_BYTE,NULL);
	volumeTex = new Texture(GL_RGBA,SCREEN_WIDTH,SCREEN_HEIGHT, GL_RGBA,GL_UNSIGNED_BYTE,NULL);
	framebuffer = new Framebuffer(SCREEN_WIDTH,SCREEN_HEIGHT,backgroundTex->ID);

	// Tell glut where the display function is
	glutDisplayFunc(MainLoop);
	glutIdleFunc(MainLoop);

	//////For display Realsense depth in 3D for test
	////glutMouseFunc(mouse);  
	////glutMotionFunc(motion);  
	////glutSpecialFunc(special);  



	// Begin infinite event loop
	glutMainLoop();

	///* Creates an instance of the PXCSenseManager */
	//   PXCSenseManager *pp = PXCSenseManager::CreateInstance();
	//   if (!pp) {
	//       wprintf_s(L"Unable to create the SenseManager\n");
	//       return 3;
	//   }

	/* Collects command line arguments */
	//    UtilCmdLine cmdl(pp->QuerySession());
	//    if (!cmdl.Parse(L"-listio-nframes-sdname-csize-dsize-isize-file-record-noRender-mirror",argc,argv)) return 3;

	//PXCCaptureManager *cm=pp->QueryCaptureManager();
	//pxcStatus sts;
	//UtilRender renderc(L"Color"), renderd(L"Depth"), renderi(L"IR");
	//PXCVideoModule::DataDesc desc={};

	//desc.deviceInfo.streams = PXCCapture::STREAM_TYPE_COLOR | PXCCapture::STREAM_TYPE_DEPTH;
	//pp->EnableStreams(&desc);
	//sts = pp->Init();
	//pp->QueryCaptureManager()->QueryDevice()->SetMirrorMode( PXCCapture::Device::MirrorMode::MIRROR_MODE_DISABLED );

	//do {	        
	//	/* Waits until new frame is available and locks it for application processing */
	//	sts=pp->AcquireFrame(false);

	//	if (sts<PXC_STATUS_NO_ERROR) {
	//		if (sts==PXC_STATUS_STREAM_CONFIG_CHANGED) {
	//			wprintf_s(L"Stream configuration was changed, re-initilizing\n");
	//			pp->Close();
	//		}
	//		break;
	//	}

	//	/* Render streams, unless -noRender is selected */
	//	if (TRUE) {
	//		const PXCCapture::Sample *sample = pp->QuerySample();
	//		if (sample) {
	//			if (sample->depth && !renderd.RenderFrame(sample->depth)) break;
	//			if (sample->color && !renderc.RenderFrame(sample->color)) break;
	//			if (sample->ir    && !renderi.RenderFrame(sample->ir))    break;

	//			

	//		}
	//	}
	//	/* Releases lock so pipeline can process next frame */
	//	pp->ReleaseFrame();     

	//} while (true);





	//try
	//{
	//	getRealSenseDepthImage();
	//	renderd.RenderFrame(sample->depth);
	//	image = imageData2IplImage(&dataDepth,DEPTH_WIDTH,DEPTH_HEIGHT ,1);
	//	//cvEqualizeHist( image, image );
	//	//CvMat mat_img(image );
	//	cvNormalize(image,image,255,0,CV_MINMAX);
	//	//	for(int i=0;i<DEPTH_HEIGHT;i++)
	//	//	{
	//	//		for(int j=0;j<DEPTH_WIDTH;j++)
	//	//		{
	//	//			cout<<cvGet2D(image,i,j).val[0]<<" ";
	//	//		}
	//	//		cout<<endl;
	//	//		cout<<"line: "<<i<<endl;
	//	//	}

	//	IplImage * timage = cvCreateImage(cvSize(DEPTH_WIDTH,DEPTH_HEIGHT),8,1);
	//	cvConvert(image,timage);

	//	//cvCvtColor(image,image,CV_RGB2BGR);
	//	cvShowImage( "Video Test 1", image );
	//	cvShowImage( "Video Test 2", timage );
	//	cvSaveImage("F:\\t_.png",image);

	//}
	//catch (cv::Exception& e)
	//{
	//	cout<<e.msg<<endl;
	//	cvWaitKey();
	//}

	//cvWaitKey();

	if(pp)pp->Release();
	if(session)session->Release();
	if(projection)projection->Release();
	getchar();
	return 0;



}
